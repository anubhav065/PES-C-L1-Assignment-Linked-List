#include<stdio.h>
#include<stdlib.h>
struct node{
    char name[100];
    int age;
    struct node *next;
};
void insert_node(struct node **head, char *n)
{
    int i=0,ex_age=0;
    char vname[100];
    //space between name and age
    while(*(n+i)!=' ')
    {
        vname[i] = *(n+i);
        i++;   
    }
    vname[i]='\0';
    i++;//move to first int digit
    while(*(n+i)!='\0')
    {
        ex_age = ex_age*10 + (*(n+i)-'0');
        i++;
    }
    //first entry in list
    if((*head) == NULL || (*head)->age > ex_age)
    {
        struct node *temp= (struct node*)malloc(sizeof(struct node));
        struct node *cpy;
        cpy = *head;
        strcpy(temp->name, vname);
        temp->age=ex_age;
        temp->next = cpy;
        *head = temp;
    }
    //entry either in between the list or at the end of the list
    else
    {
        struct node *start, *temp, *prev;
        start = *head;
        while(start->next!=NULL && start->age < ex_age)
        {
            prev=start;
            start = start->next;
        }
        //important if there are only two entries in the list and the new entry lies between the two
        if(prev->age<ex_age && start->age>ex_age) 
        {
            temp = (struct node*)malloc(sizeof(struct node));
            strcpy(temp->name, vname);
            temp->age = ex_age;
            prev->next = temp;
            temp->next = start;
        }
        //entry at the end of list
        else if(start->next==NULL)
        {
            temp = (struct node*)malloc(sizeof(struct node));
            strcpy(temp->name, vname);
            temp->age = ex_age;
            temp->next=NULL;
            start->next = temp;
        }
    }
}
void remove_node(struct node **head, char *n)
{
    int i=0,index=0;
    struct node *temp = (struct node*)malloc(sizeof(struct node));
    struct node *start = (struct node*)malloc(sizeof(struct node));
    //get the index of entry to be removed from command
    while(*(n+i)!='\0')
    {
        index = index*10 + (*(n+i)-'0');
        i++;
    }
    if(index==1)
    {
        temp = *head;
        (*head) = (*head)->next;
        free(temp);
    }
    else
    {
        i=2;
        start = *head;
        while(start->next!=NULL && i!=index)
        {
            start=start->next;
            i++;
        }
        if(i==index)
        {
            temp = start->next;
            start->next = start->next->next;
            free(temp);
        }
    }
}
void print_node(struct node *head, char *n)
{
    int i=0, index=0;
    //get the index of entry to be printed from command
    while(*(n+i)!='\0')
    {
        index = index*10 + (*(n+i)-'0');
        i++;
    }
    struct node *temp = (struct node*)malloc(sizeof(struct node));
    temp = head;
    while(temp->next!=NULL && i!=index)
    {
        temp = temp->next;
        i++;
    }
    if(i==index)
    {
        printf("%s %d\n",temp->name, temp->age);
    }
}
int main()
{
    char command[100];
    struct node *head=NULL;
    while(1)
    {
        printf("Insert, remove, print node, or stop the program?\n");
        gets(command);
        if(command[0]=='i'||command[0]=='I')
        {
            insert_node(&head, &command[7]);
        }
        else if(command[0]=='r'||command[0]=='R')
        {
            remove_node(&head, &command[7]);
        }
        else if(command[0]=='p'||command[0]=='P')
        {
            print_node(head ,&command[6]);
        }
        else if(command[0]=='s'||command[0]=='S')
        {
            break;
        }
        else
        {
            printf("Please enter a valid command\n");
        }
    }
    printf("finished...");
}